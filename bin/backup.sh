#!/usr/bin/env bash

# user options
backup_name="backup"
target_path=$PWD
cache_dir="$HOME/.cache"

# array variables to setup all the files selected
declare -A FileArr=()
declare -i idx=0

# useful constants UwU
DATE=$(date "+%d%m%Y-%H%M%S")

# help - show a help msg and stop the script
# @param (int) value of the exit code 
# @return (nil)
function help {
    echo "usage: $(basename ${0}) [OPTIONS]"
    echo "backup tool to archive multiple directories at once"
    echo
    echo "arguments:"
    echo "  -h          show this help message"
    echo "  -n [NAME]   name of the backup archive, default is 'backup'"
    echo "  -f [FILE]   path to the file/directory to backup"
    echo "  -t [TARGET] target path to store the backup archive"
    exit ${1}
}

# parser - get option from the commandline argument
# @return (nil)
function parser {
    while getopts ":n:f:t:c:h" opt; do
        case $opt in
            t) target_path=$OPTARG ;;
            n) backup_name=$OPTARG ;;
            c) cache_dir=$OPTARG ;;
            f)
                FileArr[${idx}]=$OPTARG
                idx+=1
                ;;
            h) help 0 ;; # actualy, show the help
            ?) help 1 ;; # invalid argument
        esac
    done
}

parser $@
backup_name="${backup_name}_$DATE"

# display all the configured options
echo "[-n] archive name: $backup_name"
echo "[-t] save to:      $target_path"
echo "[-c] cache dir:    $cache_dir"
echo

# check if all the selected files exists
for file in ${FileArr[@]}; do
    echo -n "checking $file ..."
    if [ ! -e $file ]; then
        echo -e "\nError: cannot find $file, no such file or directory"
        exit 1
    fi
    echo "done"
done

# check the other files
if [ ! -d $target_path ]; then
    echo "Error: cannot find $target_path, no such directory"
elif [ ! -d $cache_dir ]; then
    echo "Error: cannot find $cache_dir, no such directory"
fi

echo -e "\nstarting the backup process!\n"

# copy all the files to the cache dir
cache_name="${cache_dir%/}/$backup_name"
mkdir $cache_name

for file in ${FileArr[@]}; do
    echo "copping: $file"
    cp -rf $file $cache_name
done
echo

# finaly, create the backup
cd $cache_dir
tarxz_file="$backup_name.tar.xz"

GZIP=-9 tar -pvcJf $tarxz_file $(basename $cache_name)
mv $tarxz_file $target_path

# delete the cache directory after all that
echo -e "\nDone!\n"
rm -rf "${cache_dir%/}/$backup_name"

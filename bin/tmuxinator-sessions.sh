#!/usr/bin/env bash

TMUXINATOR_DIR="$HOME/.config/tmuxinator"
FILE=$(ls $TMUXINATOR_DIR | rofi -dmenu)

tmuxinator $(echo $FILE | cut -d. -f1)
